<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class Module extends Model
{
    protected $table = 'modules';
    protected $primaryKey = 'id';
    public $timestamps = true;
     
    
    /* Get List */
    public function get_list($status='')
    {
        $query = DB::table("$this->table as a");
        
        if(app()->getLocale()=='id'){
            $query = $query->select('a.*');
        }else{
            $query = $query->select('a.*','a.title_en as title');
        }
        
        if($status!='')
            $query = $query->where("a.status",'=',$status);
        
        $rs = $query->get();

        if($query->count()!=NULL)
            return $rs;
        else
            return NULL;
    }


    /* Get Detail Data */
    public function get_detail($id)
    {
        $query = DB::table("$this->table as a")
            ->select('a.*')
            ->where($this->primaryKey,$id);        
        $row = $query->first();

        if($query->count()!=NULL)
            return $row;
        else
            return NULL;
    }


    /* Get Detail Data By Name */
    public function get_detail_by_name($name)
    {
        $query = DB::table("$this->table as a");
        
        if(app()->getLocale()=='id'){
            $query = $query->select('a.*');
        }else{
            $query = $query->select('a.*','a.title_en as title');
        }
        
        $query = $query->where('name',$name);        
        $row = $query->first();

        if($query->count()!=NULL)
            return $row;
        else
            return NULL;
    }

    
}