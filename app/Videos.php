<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class Banner extends Model
{
    protected $table = "banner";
    protected $primaryKey = 'id';
    public $timestamps = true;


    /* Get List */
    public function get_list($status='', $limit='')
    {
        $query = DB::table("$this->table as a")
            ->select('a.*')
            ->join('menu as b','b.id','a.menu')
            ->orderBy("b.sort_no","ASC");
        
        if($status!='')
            $query = $query->where("a.status",'=',$status);

        if($limit!='')
            $query = $query->limit($limit);
        
        $rs = $query->get();

        if($query->count()!=NULL)
            return $rs;
        else
            return NULL;
    }


    /* Get Detail Data */
    public function get_detail($id)
    {
        $query = DB::table("$this->table as a")
            ->select('a.*')
            ->where($this->primaryKey,$id);        
        $row = $query->first();

        if($query->count()!=NULL)
            return $row;
        else
            return NULL;
    }
}
