<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class Menu extends Model
{
    protected $table = "menu";
    protected $primaryKey = 'id';
    public $timestamps = true;


    /* Get List */
    public function get_list($frontend_status='', $limit='')
    {
        $query = DB::table("$this->table as a");
            
        
        if(app()->getLocale()=='id'){
            $query = $query->select('a.*');
        }else{
            $query = $query->select('a.*','a.name_en as name');
        }

        $query = $query->orderBy("a.sort_no","ASC");

        if($frontend_status!='')
            $query = $query->where("a.frontend_status",'=',$frontend_status);

        if($limit!='')
            $query = $query->limit($limit);
        

        $rs = $query->get();

        if($query->count()!=NULL)
            return $rs;
        else
            return NULL;
    }


    /* Get Detail Data */
    public function get_detail($id)
    {
        $query = DB::table("$this->table as a")
            ->select('a.*')
            ->where($this->primaryKey,$id);        
        $row = $query->first();

        if($query->count()!=NULL)
            return $row;
        else
            return NULL;
    }
}
