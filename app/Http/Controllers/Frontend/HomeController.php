<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\VisionMission;
use App\Event;
use App\Menu;

class HomeController extends Controller
{
  private $ctrl = "home";
  private $title = "Home";

  /**
   * Display index page
   *
	 * @return \Illuminate\Http\Response
   */
  function index(VisionMission $vm, Event $event) 
  {
    // Get data
    $vm = $vm->get_list();
    $event = $event->get_list('',4);


    // Set data
    $data['content_view'] = "frontend.$this->ctrl.index";
    $data['page_title'] = $this->title;
    $data['menu'] = 'home';
    $data['ctrl'] = 'home';
    $data['vm'] = $vm;
    $data['event'] = $event;

    // Load view
    return view($data['content_view'], $data);
  }

}
