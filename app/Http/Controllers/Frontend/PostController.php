<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Module;
use App\Post;
use App\Category;
use App\Project;
use App\ProjectDetail;
use App\ProjectStatus;
use App\VisionMission;
use App\Event;
use Mail;

class PostController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
   * Display index page
   *
	 * @param  String $module_name 
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Module  $module
	 * @param  \App\Post  $post_model
	 * @return \Illuminate\Http\Response
   */
	public function index($module_name, Request $request, Module $module, Post $post_model,VisionMission $vm, Event $event) 
	{
		// Get module properties from table
		$module = $module->get_detail_by_name($module_name);
	
		
    
		
		// Set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$pk = $module->table_id;
		$table_option = explode(",",$module->table_option);
		$table = $module->table_name;
		$menu = $module->menu;

		// Instantiate model class
		$model_class = new $model;

		// Get data
		$list = $model_class->get_list();

		if($ctrl=='vision-mission'){
			
			$vm = $vm->get_list();
			$event = $event->get_list('',4);
			$data['vm'] = $vm;
			$data['event'] = $event;
		}

		// set data view
		$data[ 'content_view' ] = "frontend.$ctrl.index";
		$data[ 'page_title' ] = $title;
		$data[ 'ctrl' ] = $ctrl;
		$data[ 'title' ] = $title;
		$data[ 'table_option' ] = $table_option;
		$data[ 'pk' ] = $pk;
		$data[ 'list' ] = $list;
		$data[ 'menu' ] = $menu;

		// load view
		return view( $data[ 'content_view' ], $data );
	}


	/*=== Detail Page ===*/
	public function detail($module_name, $id, Request $request, Module $module, Post $post_model)
	{	

		
			
		// Get module properties from table
		if($module_name=='project-status'){
			//$status_project = ProjectStatus::find($id);
			$module = $module->get_detail_by_name('project-profile');
		//	$id=$status_project->project_id;
			$data['project_status_list'] = ProjectStatus::where('project_id','=',$id)->get(); 
		}
		else
			$module = $module->get_detail_by_name($module_name);
		

		// Set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$pk = $module->table_id;
		$table_option = explode(",",$module->table_option);
		$table = $module->table_name;
		$menu = $module->menu;

		// Instantiate model class
		$model_class = new $model;

		// Get data
		$detail = $model_class->get_detail($id);

		// set data view
		if($module_name=='project-status')
			$data[ 'content_view' ] = "frontend.$module_name.detail";
		else
			$data[ 'content_view' ] = "frontend.$ctrl.detail";
		$data[ 'page_title' ] = $title;
		$data[ 'ctrl' ] = $ctrl;
		$data[ 'title' ] = $title;
		$data[ 'table_option' ] = $table_option;
		$data[ 'pk' ] = $pk;
		$data[ 'detail' ] = $detail;
		$data[ 'menu' ] = $menu;
		if($ctrl=='project-profile'){
			$data['project_detail_list'] = ProjectDetail::where('project_id','=',$id)->get(); 
		}
		


		// load view
		return view( $data[ 'content_view' ], $data );
		
	}


	/**
   * Display index page
   *
	 * @return \Illuminate\Http\Response
   */
  function projectTimeline($id) 
  {
    
    // Set data
		$data['content_view'] = "frontend.project-status.timeline";
		$data['project_detail_list'] = ProjectStatus::where('project_id','=',$id)->orderBy('year','ASC')->get(); 
    
    // Load view
    return view($data['content_view'], $data);
  }


}