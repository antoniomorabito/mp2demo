<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Module;
use App\Post;
use App\Category;
use App\Project;
use App\Menu;
use Mail;

class PostController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
			$this->middleware('auth:admin');
	}

	/*== List Data ==*/
	public function index($module_name, Request $request, Module $module, Post $post_model) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$pk = $module->table_id;
		$table_option = explode(",",$module->table_option);
		$table = $module->table_name;
		$menu = $module->menu;

		// instantiate model class
		$model_class = new $model;
		// get data list
		if($ctrl=='news'){
			$list = $model_class->get_list_paginate('','');
		
			/*if(Session::get('user_type')==1)
				$list = $model_class->get_list_paginate('','');
			else
				$list = $model_class->get_list_paginate('',Session::get('user_id'));
			*/
		}
		elseif($ctrl=='menu')
			$list = $model_class->get_list(1);
		else
			$list = $model_class->get_list();

		$page = @$request['page'];
		if($page==0)
			$page=1;

		// set data view
		$data[ 'content_view' ] = "backend.template.index";
		if($ctrl=='gallery'){
			$cat = $request->cat;
			if($cat=='' || $cat=='photo'){
				$title = 'Foto';
				$cat='photo';
			}else{
				$title = 'Video';
				$cat='video';
			}
			$data[ 'content_view' ] = "backend.template.gallery";
			$list = $post_model->get_list($table,$pk,'','','','',$cat);
			$data[ 'cat' ] = $cat;
		}
		if($ctrl=='publication')
			$data[ 'content_view' ] = "backend.template.photobox";
		if($ctrl!='about')	
			$data[ 'page_title' ] = $title;
		else
			$data[ 'page_title' ] = $title;
		$data[ 'ctrl' ] = $ctrl;
		$data[ 'title' ] = $title;
		$data[ 'table_option' ] = $table_option;
		$data[ 'pk' ] = $pk;
		$data[ 'list' ] = $list;
		$data[ 'page' ] = $page;
		$data[ 'menu' ] = $menu;

		// load view
		return view( $data[ 'content_view' ], $data );

	}


	/*=== Detail Page ===*/
	public function detail($id)
	{	
		// init data
		$detail = News::find($id);
		
		// set data for view
		$data['content_view'] = "backend.$ctrl.detail";
		$data['detail'] = $detail;
		
		// load view
		return view( $data[ 'content_view' ], $data );
		
	}


	/*== Create Page ==*/
	public function create($module_name, Request $request, Module $module, Category $category, Project $project, Menu $menu_model) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		$module_list = Module::whereIn('name', ['training-material','article','scientific-publication','msc'])->get();
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$data_option = explode(",",$module->data_option);
		$menu = $module->menu;
		if(in_array("category", $data_option)){
			$category_list = Category::where('module','=',$module->module_id)->get();
			$data['category_list'] = $category_list;
		}
		if(in_array("project", $data_option)){
			$project_list = $project->get_list();
			$data['project_list'] = $project_list;
		}
		if(in_array("menu", $data_option)){
			$menu_list = $menu_model->get_list(1);
			$data['menu_list'] = $menu_list;
		}
		
		

		// set data view
		$data[ 'content_view' ] = "backend.template.form";
		$data[ 'page_title' ] = "Tambah ".$title;
		$data[ 'ctrl' ] = $ctrl;
		$data[ 'title' ] = $title;
		$data[ 'data_option' ] = $data_option;
		$data[ 'menu' ] = $menu;
		$data[ 'module_list' ] = $module_list;

		// load view
		return view( $data[ 'content_view' ], $data );

	}


	/*== Update Form ==*/
	public function update($module_name, $id, Request $request, Module $module, Project $project, Menu $menu_model) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		$module_list = Module::whereIn('name', ['training-material','article','scientific-publication','msc'])->get();

		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$pk = $module->table_id;
		$data_option = explode(",",$module->data_option);
		$menu = $module->menu;
		if(in_array("category", $data_option)){
			$category_list = Category::where('module','=',$module->module_id)->get();
			$data['category_list'] = $category_list;
		} 

		if(in_array("project", $data_option)){
			$project_list = $project->get_list();
			$data['project_list'] = $project_list;
		}
		if(in_array("menu", $data_option)){
			$menu_list = $menu_model->get_list(1);
			$data['menu_list'] = $menu_list;
		}
		
		

		// instantiate model class
		$model_class = new $model;
		// get detail
		$detail = $model_class->get_detail($id);
	
		// set data view
		$data[ 'content_view' ] = "backend.template.form";
		$data[ 'page_title' ] = "Update ".$title;
		$data[ 'ctrl' ] = $ctrl;
		$data[ 'title' ] = $title;
		$data[ 'detail' ] = $detail;
		$data[ 'pk' ] = $pk;
		$data[ 'data_option' ] = $data_option;
		$data[ 'menu' ] = $menu;
		$data[ 'module_list' ] = $module_list;

		// load view
		return view( $data[ 'content_view' ], $data );

	}


	/*== Save ==*/
	public function save($module_name, Request $request, Module $module) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$data_option = explode(",",$module->data_option);
		

		// set param
		$id = $request->id;
		
		// cek data
		if($id=="") 
		{
			// create new
			$data = new $model;
			$data->created_by = 1;//Session::get('user_id');
			$data->created_at = date("Y-m-d H:i:s");
			$data->updated_by = 1;//Session::get('user_id');
			$data->updated_at = date("Y-m-d H:i:s");
			if($ctrl=='news'){
				$data->headline='';
			}
		} 
		else 
		{
			// update
			$data = $model::find($id);
			$data->updated_by = 1;//Session::get('user_id');
			$data->updated_at = date("Y-m-d H:i:s");
			
				
		}

		if($ctrl=='news'){
			$data->from_wp=0;
		}


		if(in_array("image", $data_option))
		{
			if($request->file('image')!="")
			{
				// destination path
				$destination_path = public_path("images/$ctrl/");

				// upload foto profil
				$file_foto = $request->file('image');
				$md5_name = uniqid().md5_file($file_foto->getRealPath());
				$ext = $file_foto->getClientOriginalExtension();
				$file_foto->move($destination_path,"$md5_name.$ext");

				//set data
				$data->image = "$md5_name.$ext";
					
			}
		}

		if(in_array("file", $data_option))
		{
			if($request->file('file')!="")
			{
				// destination path
				$destination_path = public_path('docs/'.$ctrl.'/');

				// upload foto profil
				$file_foto = $request->file('file');
				$md5_name = uniqid().md5_file($file_foto->getRealPath());
				$ext = $file_foto->getClientOriginalExtension();
				$file_foto->move($destination_path,"$md5_name.$ext");

				//set data
				$data->file = "$md5_name.$ext";
				
			}

		}

		// set data
		
		// set data
		if(in_array("category", $data_option)){
			$data->category = $request->category;
		}

		if(in_array("event_date", $data_option)){
			$data->start_date = $request->date_start;
			$data->end_date = $request->date_end;
		}

		if(in_array("menu", $data_option)){
			$data->menu = $request->menu;
		}

		if(in_array("location", $data_option)){
			$data->location = $request->location;
		}
		
		if(in_array("year", $data_option)){
			$data->year = $request->year;
		}

		if(in_array("project", $data_option)){
			$data->project_id = $request->project;
		}

		if(in_array("dot_color", $data_option)){
			$data->dot_color = $request->dot_color;
		}

		if(in_array("module", $data_option)){
			$data->module = $request->module_id;
		}

		if(in_array("title", $data_option)){
			$data->title = $request->title;
			$data->title_en = $request->title_en;
		}

		if(in_array("name", $data_option)){
			$data->name = $request->name;
			$data->name_en = $request->name_en;
		}
		
		if(in_array("type", $data_option))
			$data->type = $request->type;

		if($request->type!='')
			$data->type = $request->type;
		
		if(in_array("video_url", $data_option))
			$data->video_url = $request->video_url;

		if(in_array("pilar", $data_option))
			$data->pilar = $request->pilar;

		if(in_array("url_view", $data_option))
			$data->url_view = $request->url_view;

		if(in_array("question", $data_option))
			$data->question = $request->question;

		if(in_array("answer", $data_option))
			$data->answer = $request->answer;

		if(in_array("content_short", $data_option)){
			$data->content_short = $request->content_short;
			$data->content_short_en = $request->content_short_en;
		}
			
		if($ctrl!='contact'){
			if(in_array("content_full", $data_option)){
				$data->content_full = $request->content_full;
				$data->content_full_en = $request->content_full_en;
			}
				
		}	
		
		if($ctrl=='contact'){
			$data->status = 1;
		}

		if(in_array("status", $data_option))
			$data->status = $request->status;
		

		// save
		$data->save();

		if($ctrl=='contact')
		{
			
			$email = $data->email;
			$subject = "Re: ".$data->subject;
			$content = $data->content_full ;
			$name = $data->name;
			$answer = $request->answer;

			$data = array( 	
							'name' => $name,
							'content' => $content,
							'answer' => $answer,
						);

			// send mail
			Mail::send(['html' =>'emails.contactus'], $data, function($mail) use ($email,$name,$subject)  {
				$mail->to($email)
						->replyTo('stbmnasional@gmail.com', 'STBM Nasional')
						->subject($subject)
						->from('stbmnasional@gmail.com','STBM Nasional');
			});
			
		}
		
		// notification 
		$status = "success";
		$message = "Data telah berhasil disimpan";
		$request->session()->flash( 'status', $status );
		$request->session()->flash( 'message', $message );
		
		// redirect page
		return redirect("admin/post/".$ctrl);

	}


	/*== Save Status ==*/
	public function save_status($module_name, $id, $status, Request $request, Module $module) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$data_option = explode(",",$module->data_option);
		
		// update
		$data = $model::find($id);
		$data->updated_by = 1;
		$data->updated_at = date("Y-m-d H:i:s");
		
		$data->status = $request->status;
		
		// save
		$data->save();
		
		// notification 
		$status = "success";
		$message = "Data has been saved";
		$request->session()->flash( 'status', $status );
		$request->session()->flash( 'message', $message );
		
		// redirect page
		return redirect("admin/app/".$ctrl);

	}


	/*== Save Headline ==*/
	public function save_headline($module_name, $id, $headline, Request $request, Module $module) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$data_option = explode(",",$module->data_option);
		

		if($request->headline==0)
			$headline = '';
		else
			$headline = $request->headline;


		// ubah headline sebelumnya
		$data = $model::where('headline','=',$headline)->first();
		if($data!='')
		{
			$data->headline = '';
			$data->save();
		}
		

		// update
		$data = $model::find($id);
		$data->updated_by = 1;
		$data->updated_at = date("Y-m-d H:i:s");
		
		
		
		$data->headline = $headline;
		
		// save
		$data->save();

		

		
		// notification 
		$status = "success";
		$message = "Data has been saved";
		$request->session()->flash( 'status', $status );
		$request->session()->flash( 'message', $message );
		
		// redirect page
		return redirect("admin/app/".$ctrl);

	}


	/*== Delete ==*/
	public function delete($module_name, $id, Request $request, Module $module) 
	{
		// get module properties from table
		$module = $module->get_detail_by_name($module_name);
		// set module properties
		$ctrl = $module->name;
		$title = $module->title;
		$model = "App\\".$module->model;
		$data_option = explode(",",$module->table_option);

		// delete
		$data = $model::find($id)->delete();

		// notification 
		$status = "success";
		$message = "Data telah berhasil dihapus";
		$request->session()->flash( 'status', $status );
		$request->session()->flash( 'message', $message );

		// redirect page
		return redirect("admin/post/".$ctrl);

	}



}