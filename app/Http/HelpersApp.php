<?php

  function dateCustom($string,$format)
  {
      return \Carbon\Carbon::parse($string)->format($format);
  }

 
  
  /**
   * Menampilkan Nama Project
   */
  function getProjectName($id)
  {
    $project = new App\Project;
    $detail= $project->get_detail($id);
    $name = $detail->location_name;
    return $name;
  }

  /**
   * Menampilkan Nama Project
   */
  function getMenuName($id)
  {
    $menu = new App\Menu;
    $detail= $menu->get_detail($id);
    $name = $detail->name;
    return $name;
  }


  /**
   * Menampilkan Nama Project
   */
  function getMenu($frontend_status)
  {
    $menu = new App\Menu;
    $list = $menu->get_list($frontend_status);

    return $list;
  }

  /**
   * Menampilkan Nama Project
   */
  function getC6()
  {
    $menu = new App\C6;
    $list = $menu->get_list();

    return $list;
  }


  /**
   * Menampilkan Nama Project
   */
  function getBanner($menu_id)
  {
    $menu = new App\Banner;
    $list = $menu->get_detail_by_menu($menu_id);
   
    
    return $list;
  }

  



  

  