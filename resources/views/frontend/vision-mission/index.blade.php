@extends('frontend.layout')

@section('content')


  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="background-image: url('{{ asset("img/visimisi-banner.png") }}')">
      </div>
    </div>
  </header>


 <!-- Visi Misi -->
 <div class="container-fluid visi-misi pb-5">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row  py-3 py-md-5">
        <div class="col-lg-5 pt-5">
          <div class="row">
              <div class="text-center mb-4 col-12 col-md-2">
                <img src="{{ asset('images/vision-mission/'.$list[1]->image) }}" class="img-fluid" />
              </div>
              <div class="text-center text-md-left col-12 col-md-10 pl-4">
                <h2>{{ $list[1]->title }}</h2>
                <p class="font-size-big text-left ">{!! $list[1]->content_full !!}</p>
              </div>
          </div>
        </div>
        <div class="col-12 col-lg-2 text-center">
            <img src="{{ asset('img/home-line.png') }}" class="d-none d-lg-inline" />
            <hr class="d-block d-lg-none my-5">
        </div>
        <div class="col-lg-5 pt-0 pt-md-5">
            <div class="row">
                <div class="text-center mb-4 col-12 col-md-2">
                    <img src="{{ asset('images/vision-mission/'.$list[0]->image) }}" class="img-fluid" />
                </div>
                <div class="text-center text-md-left col-md-10 pl-4">
                  <h2>{{ $list[0]->title }}</h2>
                  <p class="font-size-big text-left ">{!! $list[0]->content_full !!}</p>
                </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- / Visi Misi -->


 
  
  <!-- 6C -->
  <div class="container-fluid home-6c py-5">
    @php $c6 = getC6(); @endphp
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-3 py-md-5">
  
        <div class="col-md-12">
          <div >
            <h2 class="mb-5 text-center">{{ $c6[0]->title }}</h2>
            <div class="font-size-big text-left ">{!! $c6[0]->content_full !!}</div>
          </div>
        </div>
  
      </div>
  
      <div class="row mb-4">
        <div class="col-12 col-lg-4 mb-4">
          <div class="font-size-big text-left"><strong>{{ $c6[1]->title }}</strong>
            <br><br>
            {!! $c6[1]->content_full !!}  
          </div>
        </div>
        <div class="col-12 col-lg-4 mb-4">
            <div class="font-size-big text-left"><strong>{{ $c6[2]->title }}</strong>
              <br><br>
              {!! $c6[2]->content_full !!}  
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4">
            <div class="font-size-big text-left"><strong>{{ $c6[3]->title }}</strong>
              <br><br>
              {!! $c6[3]->content_full !!}  
            </div>
        </div>
      {{-- </div>
  
      <div class="row mb-3 mb-md-5"> --}}
        <div class="col-12 col-lg-4 mb-4">
            <div class="font-size-big text-left"><strong>{{ $c6[4]->title }}</strong>
              <br><br>
              {!! $c6[4]->content_full !!}  
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4">
            <div class="font-size-big text-left"><strong>{{ $c6[5]->title }}</strong>
              <br><br>
              {!! $c6[5]->content_full !!}  
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4">
            <div class="font-size-big text-left"><strong>{{ $c6[6]->title }}</strong>
              <br><br>
              {!! $c6[6]->content_full !!}  
            </div>
        </div>
      </div>
  
    </div>
  </div>
  <!-- / 6C  -->
  
  
  <!-- Upcoming Event -->
  <div class="container-fluid upcoming-event py-5">
    <div class="container pb-5">
      <!-- Call to Action Section -->
      <div class="row py-3 py-md-5">
        <div class="col-md-12">
          <h2 class="mb-5 text-center">{{ __('menu.upcoming-events') }}</h2>
        </div>
      </div>
      <ul class="list-unstyled">
        @foreach($event as $e)
        <li class="media row">
          <div class="col-12 col-md-4">
            <img src="{{ asset('images/event/'.$e->image) }}" class="mr-3 w-100 img-thumbnail mb-3 mb-md-0" />
          </div>
          <div class="col-12 media-body col-md-8">
            <h5 class="mt-0 mb-3">{{ $e->title }}</h5>
            <p><i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;{{ dateCustom($e->start_date,"M, d Y") }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;{{ $e->location }}</p>
            {!! $e->content_short !!}
            {{-- <a class="mt-3"><a href="#" class="text-white">Selengkapnya&nbsp;&nbsp;→</a></a> --}}
          </div>
        </li>
        <hr>
        @endforeach
      </ul>
    </div>
  </div>
  <!-- / Upcoming Event  -->
  
  
  
   <div class="container-fluid home-video">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5">
        <div class="col-md-12 shadow-lg m-0 p-0">
         
          <video width="100%" height="auto" controls poster="{{ asset('img/video-thumb.jpg') }}">
            <source src="{{ asset('videos/company_profile_pt_pertamina_ep.mp4') }}" type="video/mp4">
          Your browser does not support the video tag.
          </video>          
         
        </div>
      </div>
      <div class="row py-5">
          <div class="col-md-12 text-center">
          <a href="javascript:window.history.back()" class="btn btn-warning">{{ __('button.back') }}</a>
          </div>
        </div>
    </div>
  </div>

  


  
@endsection
