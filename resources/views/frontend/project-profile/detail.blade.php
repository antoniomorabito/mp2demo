@extends('frontend.layout')

@section('content')

<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(3) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>

  <!-- Profil Proyek -->
  <div class="container-fluid profil-proyek">
      <div class="container">
        <!-- Call to Action Section -->
        <div class="row py-5 text-center">
          <div class="col-md-12 py-0 py-md-5 text-center">
              <h2 class="mb-md-5">{{ $detail->name }}</h2>
          </div>
          
        </div>
      </div>
  
    </div>
  
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <a data-fancybox="" href="{{ asset("images/project-profile/$detail->image")}}"><div class="carousel-inner" role="listbox">
      <div class="carousel-item active" style="background-image: url('{{ asset("images/project-profile/$detail->image")}}');height: 50vh;">
        </div></a>
      </div>
    </header>
  
    
    <!-- / Profil Proyek -->
  
  
  
    <!-- Status Proyek -->
    <div class="container-fluid lokasi-proyek">
      <div class="container">
        <!-- Call to Action Section -->
        <div class="row py-5">
          <div class="col-md-12 pl-md-5 prl-2 font-size-big">
            {!! $detail->content_full !!}
          </div>        
        </div>
  
        
  
        @php $i=1;$j=1; @endphp
        @foreach($project_detail_list as $row)
          @php
            if($i%2==1){
              echo '<div class="row justify-content-md-center">';
              if($i==1)
                $bg_class="bg-odd-odd";
              if($i==3)
                $bg_class="bg-even-odd";
            } else {
              if($i==2)
                $bg_class="bg-odd-even";
              if($i==4)
                $bg_class="bg-even-even";
            }
          @endphp
          <div class="col-md-5 rounded {{ $bg_class }}  mr-0 mb-3 mb-md-5" >
            <div class="row">
              <div class="col-3">
                <img src="{{ asset("images/project-detail/$row->image") }}" class="img-fluid" />
              </div>
              <div class="col-9 text-left">
                <p class="mb-0">{{ $row->content_short }}</p>
              </div>
            </div>
          </div>
          @php 
          if($i%2==0 or $j==$project_detail_list->count())
            echo '</div>'; 
          $i++; $j++;
          if($i==5)
            $i=1;
          @endphp
        @endforeach
                
  
               
     
        <div class="row py-5">
          <div class="col-md-12 shadow-lg m-0 p-0">
           
              <video width="100%" height="auto" controls poster="{{ asset('img/video-thumb.jpg') }}">
                <source src="{{ asset('videos/company_profile_pt_pertamina_ep.mp4') }}" type="video/mp4">
              Your browser does not support the video tag.
              </video>        
           
          </div>
        </div>
      
  
              
      <div class="row py-5">
      <div class="col-md-12 text-center">
      <a href="javascript:window.history.back()" class="btn btn-primary">{{ __('button.back') }}</a>
      </div>
      </div>
  
  
      </div>
  
      
  
    </div>
    <!-- / Status Proyek -->


  
@endsection
