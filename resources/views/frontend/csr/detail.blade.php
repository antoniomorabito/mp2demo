@extends('frontend.layout')

@section('content')

  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(8) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>

  <!-- CSR Detail -->
  <div class="container-fluid csr">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5">
        <div class="col-md-3 pr-0 pr-md-5 text-center mb-4">
          <img src="{{ asset("images/$ctrl/$detail->image") }}" class="mr-3 img-fluid img-thumbnail" />
        </div>
        <div class="col-md-9 shadow-lg p-5 mb-5 bg-white">
          <div class="row">
            <div class="col-md-12">
                <h3>{{ $detail->title }}</h3>
                <p class="mb-4">Edisi {{ $detail->year }}</p>
                <hr>
            </div>
            
            <div class="col-6">
              <h4 class="mb-5 pt-2">Synopsis</h4>
            </div>
            <div class="col-6 text-right">
              <button type="button" class="btn btn-primary">Read</button>
            </div>
            <div class="col-md-12">
              <div class="font-size-big">
              {!! $detail->content_full !!}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row pb-5">
          <div class="col-md-9 offset-md-3 text-center">
          <a href="javascript:window.history.back()" class="btn btn-primary text-white">{{ __('button.back') }}</a>
          </div>
        </div>

    </div>

  </div>
  <!-- / CSR Detail -->


  
@endsection
