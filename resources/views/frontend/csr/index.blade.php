@extends('frontend.layout')

@section('content')


  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(8) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>


  <!-- CSR -->
  <div class="container-fluid csr">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5">
        <div class="col-md-12 shadow-lg p-4 p-md-5 mb-5 bg-white">
          @foreach($list as $row)
          <a href="{{ URL("web/$ctrl/$row->id") }}">
            <div class="row">
            <div class="col-md-2 text-center"><img src="{{ asset("images/$ctrl/$row->image") }}" class="mr-md-3 mr-0 mb-3 mb-md-0 img-fluid img-thumbnail" /></div>
              <div class="col-md-10">
              <h3>{{ $row->title }}</h3>
              <p class="mb-4">Edisi {{ $row->year }}</p>
              <p class="font-size-big mb-0">{!! $row->content_short !!} ...</p>
              </div>
            </div>
          </a>
          <hr class="my-3">
          @endforeach
        </div>
      </div>

      <div class="row pb-5">
          <div class="col-md-12 text-center">
          <a href="javascript:window.history.back()" class="btn btn-primary  text-white">{{ __('button.back') }}</a>
          </div>
        </div>
    </div>
  </div>
  <!-- / CSR -->


  
@endsection
