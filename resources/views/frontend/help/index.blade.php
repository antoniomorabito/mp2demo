@extends('frontend.layout')

@section('content')

  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="background-image: url('{{ asset("img/kebijakan.png") }}')">
      </div>
    </div>
  </header>

  <!-- Kebijakan -->
  <div class="container-fluid kebijakan">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5">
        <div class="col-md-12 text-center mb-5">
        <h2 class="py-0 py-md-5 ">{{ $page_title }}</h2>
        </div>

        <div class="col-md-12">
          {!! $list[0]->content_full !!}
          <hr class="my-5">
        </div>

      </div>
      
      <div class="row pb-5">
        <div class="col-md-12 text-center">
        <a href="javascript:window.history.back()" class="btn btn-primary  text-white">{{ __('button.back') }}</a>
        </div>
      </div>

    </div>
  </div>
  <!-- / Kebijakan -->

@endsection
