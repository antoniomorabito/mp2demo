@extends('frontend.layout')

@section('content')

<header class="">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    @php $banner = getBanner(1) @endphp
    <ol class="carousel-indicators">
      @foreach($banner as $b) 
        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration-1 }}" class="@if($loop->iteration==1) active @endif"></li>
      @endforeach
      
    </ol>
    <div class="carousel-inner home" role="listbox">
      
      @foreach($banner as $b) 
        <div class="carousel-item @if($loop->iteration==1) active @endif" style="background-image: url('{{ asset("images/banner/$b->image")}}')"></div>
      @endforeach
{{--       
      <div class="carousel-item" style="background-image: url('{{ asset("img/banner.png") }}')"></div>
      <div class="carousel-item" style="background-image: url('{{ asset("img/banner.png") }}')"></div> --}}
    </div>
  </div>
</header>

<!-- Menu Mobile -->
@php $menu = getMenu(1); @endphp
<div class="container-fluid menu d-block d-lg-none py-5">
  <div class="container">
    <div class="row mb-4 mb-md-5">
      <div class="col-6 offset-0 col-md-4 offset-md-2">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
          <a href="{{ URL('web/'.$menu[1]->url) }}"><img src="{{ asset('images/menu/'.$menu[1]->image) }}" class="w-100" /> </a>
          </div>
          <div class="col-12 text-center menu-title">
          {{ $menu[1]->name }}
          </div>
        </div>
      </div>
      <div class="col-6 col-md-4">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
          <a href="{{ URL('web/'.$menu[2]->url) }}"><img src="{{ asset('images/menu/'.$menu[2]->image) }}" class="w-100" /></a>
          </div>
          <div class="col-12 text-center menu-title">
              {{ $menu[2]->name }}
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-4 mb-md-5">
      <div class="col-6 offset-0 col-md-4 offset-md-2">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
          <a href="{{ URL('web/'.$menu[3]->url) }}"><img src="{{ asset('images/menu/'.$menu[3]->image) }}" class="w-100" /></a>
          </div>
          <div class="col-12 text-center menu-title">
              {{ $menu[3]->name }}
          </div>
        </div>
      </div>
      <div class="col-6 col-md-4">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
          <a href="{{ URL('web/'.$menu[4]->url) }}"><img src="{{ asset('images/menu/'.$menu[4]->image) }}" class="w-100" /></a>
          </div>
          <div class="col-12 text-center menu-title">
              {{ $menu[4]->name }}
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-4 mb-md-5">
      <div class="col-6 offset-0 col-md-4 offset-md-2">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
              <a href="{{ URL('web/'.$menu[5]->url) }}"><img src="{{ asset('images/menu/'.$menu[5]->image) }}" class="w-100" /></a>
            </div>
            <div class="col-12 text-center menu-title">
                {{ $menu[5]->name }}
            </div>
        </div>
      </div>
      <div class="col-6 col-md-4">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
              <a href="{{ URL('web/'.$menu[6]->url) }}"><img src="{{ asset('images/menu/'.$menu[6]->image) }}" class="w-100" /></a>
            </div>
            <div class="col-12 text-center menu-title">
                {{ $menu[6]->name }}
            </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3 col-md-4 offset-md-4">
        <div class="row">
          <div class="col-8 mb-3 offset-2">
          <a href="{{ URL('web/'.$menu[7]->url) }}"><img src="{{ asset('images/menu/'.$menu[7]->image) }}" class="w-100" /></a>
          </div>
          <div class="col-12 text-center menu-title">
              {{ $menu[7]->name }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- / Menu Mobile -->


<!-- Visi Misi -->
<div class="container-fluid visi-misi d-lg-block d-none">
  <div class="container">
    <div class="row  py-3 py-md-5">
      <div class="col-md-5 pt-5">
        <div class="row">
          <div class="text-center mb-4 col-12 col-md-2">
            <img src="{{ asset('images/vision-mission/'.$vm[1]->image) }}" class="w-100" />
          </div>
          <div class="text-center text-md-left col-12 col-md-10 pl-4">
            <h2>{{ $vm[1]->title }}</h2>
            <p class="font-size-big text-left ">{!! $vm[1]->content_full !!}</p>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-2 text-center">
        <img src="{{ asset('img/home-line.png') }}" class="d-none d-sm-inline" />
        <hr class="d-block d-sm-none my-5">
      </div>
      <div class="col-md-5 pt-0 pt-md-5">
        <div class="row">
          <div class="text-center mb-4 col-12 col-md-2">
            <img src="{{ asset('images/vision-mission/'.$vm[0]->image) }}" class="w-100" />
          </div>
          <div class="text-center text-md-left col-md-10 pl-4">
            <h2>{{ $vm[0]->title }}</h2>
            <p class="font-size-big text-left ">{!! $vm[0]->content_full !!}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- / Visi Misi -->

<!-- 6C -->
<div class="container-fluid home-6c py-5 d-lg-block d-none">
  @php $c6 = getC6(); @endphp
  <div class="container">
    <!-- Call to Action Section -->
    <div class="row py-3 py-md-5">

      <div class="col-md-12">
        <div >
          <h2 class="mb-5 text-center">{{ $c6[0]->title }}</h2>
          <div class="font-size-big text-left ">{!! $c6[0]->content_full !!}</div>
        </div>
      </div>

    </div>

    <div class="row mb-4">
      <div class="col-md-4 col-lg-4">
        <div class="font-size-big text-left"><strong>{{ $c6[1]->title }}</strong>
          <br><br>
          {!! $c6[1]->content_full !!}  
        </div>
      </div>
      <div class="col-md-4 col-lg-4">
          <div class="font-size-big text-left"><strong>{{ $c6[2]->title }}</strong>
            <br><br>
            {!! $c6[2]->content_full !!}  
          </div>
      </div>
      <div class="col-md-4 col-lg-4">
          <div class="font-size-big text-left"><strong>{{ $c6[3]->title }}</strong>
            <br><br>
            {!! $c6[3]->content_full !!}  
          </div>
      </div>
    </div>

    <div class="row mb-3 mb-md-5">
      <div class="col-md-4 col-lg-4">
          <div class="font-size-big text-left"><strong>{{ $c6[4]->title }}</strong>
            <br><br>
            {!! $c6[4]->content_full !!}  
          </div>
      </div>
      <div class="col-md-4 col-lg-4">
          <div class="font-size-big text-left"><strong>{{ $c6[5]->title }}</strong>
            <br><br>
            {!! $c6[5]->content_full !!}  
          </div>
      </div>
      <div class="col-md-4 col-lg-4">
          <div class="font-size-big text-left"><strong>{{ $c6[6]->title }}</strong>
            <br><br>
            {!! $c6[6]->content_full !!}  
          </div>
      </div>
    </div>

  </div>
</div>
<!-- / 6C  -->


<!-- Upcoming Event -->
<div class="container-fluid upcoming-event py-5   d-lg-block d-none">
  <div class="container pb-5">
    <!-- Call to Action Section -->
    <div class="row py-3 py-md-5">
      <div class="col-md-12">
        <h2 class="mb-5 text-center">{{ __('menu.upcoming-events') }}</h2>
      </div>
    </div>
    <ul class="list-unstyled">
      @foreach($event as $e)
      <li class="media">
        <div class="col-md-4">
          <img src="{{ asset('images/event/'.$e->image) }}" class="mr-3 w-100 img-thumbnail" />
        </div>
        <div class="media-body col-md-8">
          <h5 class="mt-0 mb-3">{{ $e->title }}</h5>
          <p><i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;{{ dateCustom($e->start_date,"M, d Y") }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;{{ $e->location }}</p>
          {!! $e->content_short !!}
          {{-- <a class="mt-3"><a href="#" class="text-white">Selengkapnya&nbsp;&nbsp;→</a></a> --}}
        </div>
      </li>
      <hr>
      @endforeach
    </ul>
  </div>
</div>
<!-- / Upcoming Event  -->



 <div class="container-fluid home-video   d-lg-block d-none">
  <div class="container">
    <!-- Call to Action Section -->
    <div class="row py-5">
      <div class="col-md-12 shadow-lg m-0 p-0">
       
        <video width="100%" height="auto" controls poster="{{ asset('img/video-thumb.jpg') }}">
          <source src="{{ asset('videos/company_profile_pt_pertamina_ep.mp4') }}" type="video/mp4">
        Your browser does not support the video tag.
        </video>          
       
      </div>
    </div>
  </div>
</div>



<!-- Status Proyek -->
<div class="container-fluid lokasi-proyek pt-5   d-lg-block d-none">
  <!-- <div class="container"> -->
    <!-- Call to Action Section -->
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="py-5">{{ __('menu.project-location') }}</h2>
      </div>
      <div class="col-md-12 py-0"> 
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css"
  integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js"
    integrity="sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q=="
    crossorigin=""></script>

      <script type="text/javascript" src="{{ asset('map/proyek.js') }}"></script>

  <style>
  
      #mapid { 
          width: 100%;
          height: 400px; 
          margin-bottom:100px;
      }
      .leaflet-container {
  background-color:rgba(255,0,0,0.0);
}

/* css to customize Leaflet default styles  */
.custom .leaflet-popup-tip,
.custom .leaflet-popup-content-wrapper {
  background: #0a73af;
  color: #ffffff;
  text-align:center;
  font-size:16px;
 
}

.leaflet-container a.leaflet-popup-close-button{
color:#fff;
}

.leaflet-container a.leaflet-popup-close-button:hover{
color:#fff;
}

.custom .leaflet-popup-content-wrapper{
padding:10px;
padding-bottom:15px;
}

.custom .leaflet-popup-content-wrapper a{
color:#fff;
background-color:#4BC37C;
text-decoration:none;
padding:10px 15px;
border-radius:20px;
}

  </style>


                <div id="mapid"></div>
                <script>


                
                  var userIcon = L.icon({
                      iconUrl: '{{ asset('img/ellipse-marker.png') }}',
                      //shadowUrl: 'leaf-shadow.png',
                  
                      iconSize:     [25, 25], // size of the icon
                      //shadowSize:   [50, 64], // size of the shadow
                      iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
                      // shadowAnchor: [4, 62],  // the same for the shadow
                      popupAnchor:  [-13, -25] // point from which the popup should open relative to the iconAnchor
                  });

                  // create popup contents
  
  // specify popup options 
  var customOptions =
      {
      'maxWidth': '500',
      'className' : 'custom'
      }

                
                  var mymap = L.map('mapid', { zoomControl:true }).setView([-1.889306,117.917266],5);
                
                  mymap.scrollWheelZoom.disable();
                  mymap.doubleClickZoom.disable();
                
                  // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                  //               maxZoom: 9, minZoom:5,    
                  //               id: 'mapbox.streets-basic'
                  // }).addTo(mymap);

                  zoomLev = mymap.getZoom();


                  function getColor(d) {

                          // get color value
                          // if(d==0){
                          //     $color = '#FFF9E1';
                          // }else if(d>=1 && d <=25){
                          //     $color = '#FFE083';
                          // }else if(d>25 && d<=50){
                          //     $color = '#FEC107';
                          // }else if(d>50){
                          //     $color = '#FF8E01';
                          // }
                          $color = '#ddd';
                      return $color;
                      } 


                      function style(feature) {
                        return {
                        weight: 1,
                        opacity: 1,
                        color: 'white',
                        dashArray: '3',
                        fillOpacity: 0.7,
                        fillColor: getColor(feature.properties.value)
                        //color: "green"
                        };
                      }

                      function style_user(feature) {
                        return {
                            weight: 1,
                            opacity: 1,
                            color: '#D9FFF1',
                            dashArray: '0',
                            fillOpacity: 1,
                            // fillColor: getColor(feature.properties.value),
                            // color: "green"
                            fillColor:'#D9FFF1'
                        };
      }


      var geojson = L.geoJson(statesDataUser, {
          style: style_user,
          
          onEachFeature: function (feature, layer) {

              //if (feature.geometry.type === 'MultiPolygon') {
                  if(feature.properties.value ==1 ){
                
                  var bounds = layer.getBounds();
                  var center = bounds.getCenter();
                  var marker = L.marker(center,{icon:userIcon}).addTo(mymap);

                  //marker.bindPopup(feature.properties.NAME_2,customOptions);
                  // var customPopup = "Mozilla Toronto Offices<br/><img src='http://joshuafrazier.info/images/maptime.gif' alt='maptime logo gif' width='350px'/>";
                  if(feature.properties.NAME_2=='Balikpapan')
                    var pid = 1;
                  if(feature.properties.NAME_2=='Tuban')
                    var pid = 2;
                  if(feature.properties.NAME_2=='Cilacap')
                    var pid = 3;
                  if(feature.properties.NAME_2=='Balongan')
                    var pid = 4; 
                  if(feature.properties.NAME_2=='Bontang')
                    var pid = 5;

                  var customPopup = "Proyek Pembangunan<br>Pertamina "+feature.properties.NAME_2+"<br><br><a href='detail-proyek.php?pid="+pid+"'>Selengkapnya</a>";


                  
  
                  marker.bindPopup(customPopup,customOptions);

                  }       
             // }

              
          },
          
      }).addTo(mymap);
     

                
                  // var geojson = L.geoJson(statesDataUser, {
                  //     style: style,
                  
                  //     onEachFeature: function (feature, layer) {
                        
                  //         if (feature.geometry.type === 'MultiPolygon') {     

                  //             layer.bindPopup(feature.properties.state+" : "+feature.properties.value);

                  //         }
                  //     },            
                  // }).addTo(mymap);
                
                
                </script>
            
            
       

</div>
        <!-- <img src="img/indo-map.png" class="img-fluid" /> -->
      </div>

    </div>

  <!-- </div> -->

</div>
<!-- / Status Proyek -->

@endsection