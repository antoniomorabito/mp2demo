@extends('frontend.layout')

@section('content')

  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(6) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>

  <!-- Struktur Organisi -->
  <div class="container-fluid struktur-organisasi">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5">
        <div class="col-md-12 py-0 py-md-5 text-center">
          <h2 class="mb-5">{{ $list[0]->title }}</h2>
          <a class="d-inline d-lg-none" data-fancybox="" href="{{ asset("images/$ctrl/".$list[0]->image) }}"><img class="img-fluid" src="{{ asset("images/$ctrl/".$list[0]->image) }}" /></a>
          <img class="d-lg-inline d-none img-fluid" src="{{ asset("images/$ctrl/".$list[0]->image) }}" />
        </div>
      </div>

      <div class="row pb-5">
        <div class="col-md-12 text-center">
        <a href="javascript:window.history.back()" class="btn btn-warning">{{ __('button.back') }}</a>
        </div>
      </div>

    </div>

  </div>
  <!-- / Struktur Organisi  -->

@endsection