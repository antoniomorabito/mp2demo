<style>
  * {
    box-sizing: border-box;
  }
  
  body {
    background-color: #f27e46;
    font-family: Roboto, sans-serif;
    color: #222;
  }
  
  
  
  /* The actual timeline (the vertical ruler) */
  .timeline {
    position: relative;
    max-width: 1200px;
    margin: 0 auto;
  }
  
  /* The actual timeline (the vertical ruler) */
  .timeline::after {
    content: '';
    position: absolute;
    width: 6px;
    background-color: white;
    top: 0;
    bottom: 0;
    left: 50%;
    margin-left: -3px;
  }
  
  /* Container around content */
  .container {
    padding: 10px 40px;
    position: relative;
    background-color: inherit;
    width: 50%;
  }
  
  /* The circles on the timeline */
  .container::after {
    content: '';
    position: absolute;
    width: 25px;
    height: 25px;
    right: -17px;
    background-color: #C60D05;;
    border: 4px solid white;
    top: 15px;
    border-radius: 50%;
    z-index: 1;
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.25);
  }
  
  /* The circles on the timeline */
  .container .red::after {
    
    background-color: #C60D05;;
    
  }
  
  /* The circles on the timeline */
  .container.white::after {
    
    background-color: #fff;;
    
  }
  
  /* Place the container to the left */
  .left {
    left: 0;
  }
  
  /* Place the container to the right */
  .right {
    left: 50%;
  }
  
  /* Add arrows to the left container (pointing right) */
  .left::before {
    content: " ";
    height: 0;
    position: absolute;
    top: 22px;
    width: 0;
    z-index: 1;
    right: 30px;
    border: medium solid white;
    border-width: 10px 0 10px 10px;
    border-color: transparent transparent transparent white;
    
  }
  
  /* Add arrows to the right container (pointing left) */
  .right::before {
    content: " ";
    height: 0;
    position: absolute;
    top: 22px;
    width: 0;
    z-index: 1;
    left: 30px;
    border: medium solid white;
    border-width: 10px 10px 10px 0;
    border-color: transparent white transparent transparent;
  }
  
  /* Fix the circle for containers on the right side */
  .right::after {
    left: -16px;
  }
  
  /* The actual content */
  .content {
    padding: 20px 30px;
    background-color: white;
    position: relative;
    border-radius: 6px;
  }
  
  .left .content{
   text-align:right;
  }
  p{
    line-height:30px;
  }
  /* Media queries - Responsive timeline on screens less than 600px wide */
  @media screen and (max-width: 600px) {
  
    body{
      font-size:12px;
    }
    /* Place the timelime to the left */
    .timeline::after {
    left: 31px;
    }
  
    .left .content{
   text-align:left;
  }
    
    /* Full-width containers */
    .container {
    width: 100%;
    padding-left: 70px;
    padding-right: 25px;
    }
    
    /* Make sure that all arrows are pointing leftwards */
    .container::before {
    left: 60px;
    border: medium solid white;
    border-width: 10px 10px 10px 0;
    border-color: transparent white transparent transparent;
    }
  
    /* Make sure all circles are at the same spot */
    .left::after, .right::after {
    left: 15px;
    }
    
    /* Make all right containers behave like the left ones */
    .right {
    left: 0%;
    }
    p{
    line-height:20px;
  }
  }
  </style>
  </head>
  <body>
  
  <div class="timeline">
  
    
    @foreach($project_detail_list as $row)
      @if($row->status==1)
        @php $status = "red"; @endphp
      @else
        @php $status = "white"; @endphp
      @endif

      @if($loop->iteration%2==1)
        @php $align = "left";@endphp
      @else
        @php $align = "right";@endphp
      @endif
      <div class="container {{ $align." ".$status }}">
        <div class="content">
          <h2>{{ $row->year }}</h2>
          <p>{!! $row->content_short !!}</p>
        </div>
      </div>
    @endforeach  
   
   
  </div>