@extends('frontend.layout')

@section('content')


  <header>  
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(5) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>



  <!-- Status Proyek -->
  <div class="container-fluid timeline-proyek pt-5">
    <!-- <div class="container"> -->
      <!-- Call to Action Section -->
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="py-5">STATUS PROYEK</h2>
        </div>
        <div class="col-md-12 py-0"> 
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css"
    integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
    crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js"
      integrity="sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q=="
      crossorigin=""></script>

      <script type="text/javascript" src="{{ asset('map/proyek.js') }}"></script>

    <style>
    
    #mapid { 
            width: 100%;
            height: 400px; 
            margin-bottom:100px;
        }
        .leaflet-container {
    background-color:rgba(255,0,0,0.0);
}

/* css to customize Leaflet default styles  */
.custom .leaflet-popup-tip,
.custom .leaflet-popup-content-wrapper {
    background: #0a73af;
    color: #ffffff;
    text-align:center;
    font-size:16px;
   
}

.leaflet-container a.leaflet-popup-close-button{
  color:#fff;
}

.leaflet-container a.leaflet-popup-close-button:hover{
  color:#fff;
}

.custom .leaflet-popup-content-wrapper{
  padding:10px;
  padding-bottom:15px;
}


.custom .leaflet-popup-content-wrapper a{
  color:#fff;
  background-color:#f09448;
  text-decoration:none;
  padding:10px 15px;
  border-radius:20px;
}

    </style>


                  <div id="mapid"></div>
                  <script>


                  
                    var userIcon = L.icon({
                      iconUrl: '{{ asset('img/ellipse-marker.png') }}',
                        //shadowUrl: 'leaf-shadow.png',
                    
                        iconSize:     [25, 25], // size of the icon
                        //shadowSize:   [50, 64], // size of the shadow
                        iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
                        // shadowAnchor: [4, 62],  // the same for the shadow
                        popupAnchor:  [-13, -25] // point from which the popup should open relative to the iconAnchor
                    });

                    // create popup contents
    
    // specify popup options 
    var customOptions =
        {
        'maxWidth': '500',
        'className' : 'custom'
        }

                  
                    var mymap = L.map('mapid', { zoomControl:true }).setView([-1.889306,117.917266],5);
                  
                    mymap.scrollWheelZoom.disable();
                    mymap.doubleClickZoom.disable();
                  
                    // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                    //               maxZoom: 9, minZoom:5,    
                    //               id: 'mapbox.streets-basic'
                    // }).addTo(mymap);

                    zoomLev = mymap.getZoom();


                    function getColor(d) {

                            // get color value
                            // if(d==0){
                            //     $color = '#FFF9E1';
                            // }else if(d>=1 && d <=25){
                            //     $color = '#FFE083';
                            // }else if(d>25 && d<=50){
                            //     $color = '#FEC107';
                            // }else if(d>50){
                            //     $color = '#FF8E01';
                            // }
                            $color = '#ddd';
                        return $color;
                        } 


                        function style(feature) {
                          return {
                          weight: 1,
                          opacity: 1,
                          color: 'white',
                          dashArray: '3',
                          fillOpacity: 0.7,
                          fillColor: getColor(feature.properties.value)
                          //color: "green"
                          };
                        }

                        function style_user(feature) {
                          return {
                              weight: 1,
                              opacity: 1,
                              color: '#ffe4ce',
                              dashArray: '0',
                              fillOpacity: 1,
                              // fillColor: getColor(feature.properties.value),
                              // color: "green"
                              fillColor:'#ffe4ce'
                          };
        }


        var geojson = L.geoJson(statesDataUser, {
            style: style_user,
            
            onEachFeature: function (feature, layer) {

                //if (feature.geometry.type === 'MultiPolygon') {
                    if(feature.properties.value ==1 ){
                  
                    var bounds = layer.getBounds();
                    var center = bounds.getCenter();
                    var marker = L.marker(center,{icon:userIcon}).addTo(mymap);

                    //marker.bindPopup(feature.properties.NAME_2,customOptions);
                    // var customPopup = "Mozilla Toronto Offices<br/><img src='http://joshuafrazier.info/images/maptime.gif' alt='maptime logo gif' width='350px'/>";
                    if(feature.properties.NAME_2=='Balikpapan')
                      var pid = 1;
                    if(feature.properties.NAME_2=='Tuban')
                      var pid = 2;
                    if(feature.properties.NAME_2=='Cilacap')
                      var pid = 3;
                    if(feature.properties.NAME_2=='Balongan')
                      var pid = 4; 
                    if(feature.properties.NAME_2=='Bontang')
                      var pid = 5;

                      var customPopup = "Proyek Pembangunan<br>Pertamina "+feature.properties.NAME_2+"<br><br><a href='{{ URL("web/$ctrl") }}/"+pid+"'>Selengkapnya</a>";

                    
    
                    marker.bindPopup(customPopup,customOptions);

                    }       
               // }

                
            },
            
        }).addTo(mymap);
       

                  
                    // var geojson = L.geoJson(statesDataUser, {
                    //     style: style,
                    
                    //     onEachFeature: function (feature, layer) {
                          
                    //         if (feature.geometry.type === 'MultiPolygon') {     

                    //             layer.bindPopup(feature.properties.state+" : "+feature.properties.value);

                    //         }
                    //     },            
                    // }).addTo(mymap);
                  
                  
                  </script>
              
              
         

</div>
          <!-- <img src="img/indo-map.png" class="img-fluid" /> -->
        </div>

        <div class="row py-5">
          <div class="col-md-12 text-center">
          <a href="javascript:window.history.back()" class="btn btn-primary">{{ __('button.back') }}</a>
          </div>
        </div>

      </div>

    <!-- </div> -->

  </div>
  <!-- / Status Proyek -->


  
@endsection
