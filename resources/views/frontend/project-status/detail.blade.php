@extends('frontend.layout')

@section('content')

<header>  
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(5) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>


  <!-- Profil Proyek -->
  <div class="container-fluid timeline-proyek">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row py-5 text-center">
        <div class="col-md-12 pt-0 pt-md-5 text-center">
            <h2 class="mb-md-5">{{ $detail->name }}</h2>
        </div>
        
      </div>
    </div>

  </div>



  
  <!-- / Profil Proyek -->



  <!-- Status Proyek -->
  <div class="container-fluid timeline-proyek">
    <div class="container">
      <!-- Call to Action Section -->
      <div class="row pt-5">
        <div class="col-md-12 pl-md-5 prl-2">
        <iframe src="{{ URL('web/project-timeline/'.$detail->id) }}" width="100%" height="1200px" frameBorder="0" scrolling="no">
          <p>Your browser does not support iframes.</p>
        </iframe>
             
        </div>
      </div>

      <div class="row pb-5">
    <div class="col-md-12 text-center">
    <a href="javascript:window.history.back()" class="btn btn-primary">{{ __('button.back') }}</a>
    </div>
    </div>

    </div>

  </div>
  <!-- / Status Proyek -->


  
@endsection
