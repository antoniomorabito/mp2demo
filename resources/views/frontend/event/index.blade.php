@extends('frontend.layout')

@section('content')

  <link href='{{ asset('vendor/fullcalendar-4.3.1/packages/core/main.css') }}' rel='stylesheet' />
  <link href='{{ asset('vendor/fullcalendar-4.3.1/packages/daygrid/main.css') }}' rel='stylesheet' />
  <script src='{{ asset('vendor/fullcalendar-4.3.1/packages/core/main.js') }}'></script>
  <script src='{{ asset('vendor/fullcalendar-4.3.1/packages/interaction/main.js') }}'></script>
  <script src='{{ asset('vendor/fullcalendar-4.3.1/packages/daygrid/main.js') }}'></script>


  <script>
    document.addEventListener('DOMContentLoaded', function() 
    {
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
      header: { // layout header
        left: 'title', 
        center: '',
        right: 'prev,next'
      },
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '<?php echo date('Y-m-d') ?>',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        @foreach($list as $row)
        {
          title: '{{ $row->title }}',
          start: '{{ $row->start_date }}',
          end: '{{ $row->end_date }}',
          url: 'javascript:void(0)',
        },
        @endforeach
        // {
        //   title: 'Event 2',
        //   start: '2019-09-15',
        //   end: '2019-09-18',
        //   url: 'javascript:void(0)',
        // },

      ],
      // eventClick:  function(event, jsEvent, view) {
      //       $('#modalTitle').html(event.title);
      //       $('#modalBody').html(event.description);
      //       $('#eventUrl').attr('href',event.url);
      //       $('#calendarModal').modal();
      //   },
    });

      calendar.render();
    });
  </script>

  <style>
    #calendar {
      max-width: 900px;
      margin: 0 auto;
    }
  </style>


  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @php $banner = getBanner(9) @endphp
        <div class="carousel-item active" style="background-image: url('{{ asset("images/banner/".$banner[0]->image) }}')">
      </div>
    </div>
  </header>

  
  <!-- Event -->
  <div class="container-fluid event">
    <div class="container">
      <div class="row py-5">
        <div class="col-md-12">
        <!-- Calendar -->  
        <div id='calendar'></div>
        <!-- /Calendar -->
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-md-12 text-center">
        <a href="javascript:window.history.back()" class="btn btn-primary  text-white">{{ __('button.back') }}</a>
        </div>
      </div>
    </div>
  </div>
  <!-- / Event -->


    <!-- The Modal -->
  <div class="modal fade" id="calendarModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Detail Event</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 pr-2 pl-2 pr-md-5 pl-md-5">
            <h5 class="text-primary">Event Title</h5>
            <hr>
            <i class="fa fa-calendar"></i>&nbsp;&nbsp; Sept 03, 2019
            <hr>
            <p class="font-size-big">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
            <br><br>  
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div>
          </div>
        </div>
  
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Close</button>
        </div>
  
      </div>
    </div>
  </div>


  
@endsection
