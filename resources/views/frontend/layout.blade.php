<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="shortcut icon" href="{{ asset('img/p-icon.png') }}" type="image/x-icon">

  <title>PERTAMINA MEGAPROJECT - {{ $page_title }}</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet"> 
  <script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
  
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

  <!-- Fontawesome -->
  <link href="{{ asset('vendor/fontawesome/css/all.css') }}" rel="stylesheet"> <!--load all styles -->

  <!-- Styles -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

  

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light d-lg-block d-none">
    <div class="container">
      <a class="navbar-brand d-block d-md-none" href="javascript:window.history.back()"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Pertamina Mega Project</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          @php $menu = getMenu(1) @endphp
          @foreach($menu as $m)
            @if($m->url!='vision-mission' and $m->url!='project-profile' and $m->url!='project-status')
            <li class="nav-item"><a class="nav-link" href="{{ URL('web/'.$m->url) }}">{{ $m->name }}</a></li>
            @endif
            @if($m->url=='project-profile')
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('menu.project') }}</a>
                <div class="dropdown-menu animate slideIn" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ URL('web/'.$menu[2]->url) }}">{{ $menu[2]->name }}</a>
                  <a class="dropdown-item" href="{{ URL('web/'.$menu[3]->url) }}">{{ $menu[3]->name }}</a>
                </div>
              </li> 
            @endif
          
          @endforeach
          {{-- <li class="nav-item"><a class="nav-link" href="{{ URL('home') }}">{{ __('menu.home') }}</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ URL('home') }}">{{ __('menu.home') }}</a></li> --}}
          {{-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('menu.project') }}</a>
            <div class="dropdown-menu animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ URL('web/project-profile') }}">{{ __('menu.project-profile') }}</a>
              <a class="dropdown-item" href="{{ URL('web/project-status') }}">{{ __('menu.project-status') }}</a>
            </div>
          </li> --}}
          <!-- <li class="nav-item"><a class="nav-link" href="profil-proyek.php">Profil Proyek</a></li> -->
          {{-- <li class="nav-item"><a class="nav-link" href="{{ URL('web/org-structure') }}">{{ __('menu.org-structure') }}</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ URL('web/directorate-policy') }}">{{ __('menu.directorate-policy') }}</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ URL('web/csr') }}">{{ __('menu.csr') }}</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ URL('web/event') }}">{{ __('menu.calendar') }}</a></li> --}}
        </ul>
        <div class="dropdown-divider"></div>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL('web/help') }}">{{ __('menu.help') }}</a>
          </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @if(app()->getLocale()=='id')
            <img width="24px;" src="{{ asset('img/indonesia4.png') }}" />&nbsp;&nbsp;INA
            @else
            <img width="24px;" src="{{ asset('img/united-kingdom.png') }}" />&nbsp;&nbsp;EN
            @endif
          </a>
          <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ url('locale/id') }}"><img width="24px;" src="{{ asset('img/indonesia4.png') }}" />&nbsp;&nbsp;INA</a>
              <a class="dropdown-item" href="{{ url('locale/en') }}"><img width="24px;" src="{{ asset('img/united-kingdom.png') }}" />&nbsp;&nbsp;EN</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Mobile header menu -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light  d-block d-lg-none">
    <div class="container" style="font-size:14px">
      @if($ctrl=='home')
      PERTAMINA MEGAPROJECT
      @else
      <a class="text-dark" href="javascript:window.history.back()"><span class="iconify" style="margin-bottom:2px" data-icon="simple-line-icons:arrow-left" data-inline="false"></span>&nbsp;&nbsp;&nbsp;&nbsp;PERTAMINA MEGAPROJECT</a>
      @endif

      <ul class="ml-auto p-0 m-0" style="list-style-type:none">
        <li class="nav-item dropdown">
        <a style="color:#333" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @if(app()->getLocale()=='id')
          <img width="24px;" src="{{ asset('img/indonesia4.png') }}" />&nbsp;&nbsp;INA
          @else
          <img width="24px;" src="{{ asset('img/united-kingdom.png') }}" />&nbsp;&nbsp;EN
          @endif
        </a>
          <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ url('locale/id') }}"><img width="24px;" src="{{ asset('img/indonesia4.png') }}" />&nbsp;&nbsp;INA</a>
            <a class="dropdown-item" href="{{ url('locale/en') }}"><img width="24px;" src="{{ asset('img/united-kingdom.png') }}" />&nbsp;&nbsp;EN</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /Navigation -->

  <!-- Content -->
  <div id="content">
    @yield('content')
  </div>
  <!-- /Content -->

  <!-- Footer -->
  <footer class="py-4">
    <div class="container">
      <p class="m-0 text-center text-white">COPYRIGHT &copy;2019 PERTAMINA MEGAPROJECT</p>
    </div>
  </footer>
  <!-- /Footer -->

</body>

</html>