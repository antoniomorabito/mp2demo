<!doctype html>
<html lang="en">

<head>
  
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Admin Pertamina MP</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="{{ asset('img/p-icon.png') }}" type="image/x-icon">

  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/icon-kit/dist/css/iconkit.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/ionicons/dist/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css') }}">
  
  <link rel="stylesheet" href="{{ asset('backend/css/theme.min.css') }}">
  <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">
  <script src="{{ asset('backend/vendor/modernizr-2.8.3.min.js') }}"></script>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

  <script src="https://cdn.tiny.cloud/1/ep69md7kfcnua9tf2rx4yrzgwjfna5u4oovep473gxid8891/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script type="text/javascript">
  tinymce.init({
    selector: '.content_full',
    width: '100%',
    height: 500,
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table directionality emoticons template paste'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview '
  });
  </script>

</head>

<body>
  <div class="wrapper">
    <header class="header-top" header-theme="light">
      <div class="container-fluid">
        <div class="d-flex justify-content-between">
            {{-- <div class="top-menu d-flex align-items-center">
                <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                <div class="header-search">
                    <div class="input-group">
                        <span class="input-group-addon search-close"><i class="ik ik-x"></i></span>
                        <input type="text" class="form-control">
                        <span class="input-group-addon search-btn"><i class="ik ik-search"></i></span>
                    </div>
                </div>
                <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
            </div> --}}
          <div class="top-menu d-flex align-items-center">
              <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
            <div class="pl-10 font-weight-bold">PERTAMINA MEGAPROJECT</div>
          </div>
          <div class="top-menu d-flex align-items-center">
            <div class="dropdown">
              <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="{{ asset('backend/img/avatar/1493022872_avatar.jpg') }}"></a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                {{-- <a class="dropdown-item" href="#"><i class="ik ik-user dropdown-icon"></i>Profile</a>
                <a class="dropdown-item" href="#"><i class="ik ik-lock dropdown-icon"></i>Change Password</a> --}}
                <a class="dropdown-item" href="{{ URL('admin/logout') }}" href="{{ route('logout') }}"
            onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="ik ik-power dropdown-icon"></i>Logout
          </a>
            

          <form id="logout-form" action="{{ URL('admin/logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="page-wrap">
      <div class="app-sidebar colored">
        <div class="sidebar-header" style="background-color: #fff">
          <img src="{{ asset('backend/img/logo1.png') }}" class="header-brand-img w-50" alt="">
        </div>

        <div class="sidebar-content">
          <div class="nav-container pt-20">
            <nav id="main-menu-navigation" class="navigation-main">
                <div class="nav-item @if($ctrl=='home') active @endif">
                  <a href="{{ URL('admin/home') }}"><i class="ik ik-home"></i><span>Home</span></a>
                </div>
                <div class="nav-item @if($ctrl=='menu') active @endif">
                  <a href="{{ URL('admin/post/menu') }}"><i class="ik ik-grid"></i><span>Menu</span></a>
                </div>
                <div class="nav-item @if($ctrl=='banner') active @endif">
                  <a href="{{ URL('admin/post/banner') }}"><i class="ik ik-image"></i><span>Image Banner</span></a>
                </div>
                {{-- <div class="nav-item has-sub @if($ctrl=='banner') active @endif @if($ctrl=='banner') open @endif">
                  <a href="javascript:void(0)"><i class="ik ik-image"></i><span>Media</span></a>
                  <div class="submenu-content">
                      <a href="{{ URL('admin/post/banner') }}" class="menu-item @if($ctrl=='banner') active @endif">Image Banner</a>
                      <a href="{{ URL('admin/post/video') }}" class="menu-item">Video</a>
                  </div>
                </div> --}}

                <div class="nav-item @if($ctrl=='vision-mission') active @endif">
                  <a href="{{ URL('admin/post/vision-mission') }}"><i class="ik ik-target"></i><span>Visi Misi</span></a>
                </div>
                <div class="nav-item @if($ctrl=='6c') active @endif">
                  <a href="{{ URL('admin/post/6c') }}"><i class="ik ik-file-text"></i><span>6C</span></a>
                </div>
                
                {{-- <div class="nav-item @if($ctrl=='project-profile') active @endif">
                  <a href="{{ URL('admin/post/project-profile') }}"><i class="ik ik-box"></i><span>Profil Proyek</span></a>
                </div> --}}

                <div class="nav-item has-sub @if(in_array($ctrl,array("project-profile", "project-status", "project-detail"))) active open @endif">
                  <a href="javascript:void(0)"><i class="ik ik-box"></i><span>Proyek</span></a>
                  <div class="submenu-content">
                      <a href="{{ URL('admin/post/project-profile') }}" class="menu-item @if($ctrl=='project-profile') active @endif">Profil Proyek</a>
                      <a href="{{ URL('admin/post/project-detail') }}" class="menu-item @if($ctrl=='project-detail') active @endif">Detail Proyek</a>
                      <a href="{{ URL('admin/post/project-status') }}" class="menu-item @if($ctrl=='project-status') active @endif"">Status Proyek</a>
                  </div>
                </div>
                {{-- <div class="nav-item @if($ctrl=='project-detail') active @endif">
                  <a href="{{ URL('admin/post/project-detail') }}"><i class="ik ik-grid"></i><span>Detail Proyek</span></a>
                </div>
                <div class="nav-item @if($ctrl=='project-status') active @endif">
                  <a href="{{ URL('admin/post/project-status') }}"><i class="ik ik-trending-up"></i><span>Status Proyek</span></a>
                </div> --}}
                <div class="nav-item  @if($ctrl=='org-structure') active @endif">
                  <a href="{{ URL('admin/post/org-structure') }}"><i class="ik ik-git-pull-request"></i><span>Struktur Organisasi</span></a>
                </div>
                <div class="nav-item  @if($ctrl=='csr') active @endif">
                  <a href="{{ URL('admin/post/csr') }}"><i class="ik ik-book-open"></i><span>CSR</span></a>
                </div>
                <div class="nav-item @if($ctrl=='directorate-policy') active @endif">
                  <a href="{{ URL('admin/post/directorate-policy') }}"><i class="ik ik-feather"></i><span>Kebijakan Direktorat</span></a>
                </div>
                <div class="nav-item @if($ctrl=='event') active @endif">
                  <a href="{{ URL('admin/post/event') }}"><i class="ik ik-calendar"></i><span>Calendar</span></a>
                </div>
                <div class="nav-item @if($ctrl=='help') active @endif">
                  <a href="{{ URL('admin/post/help') }}"><i class="ik ik-help-circle"></i><span>Help</span></a>
                </div>
            </nav>
          </div>
        </div>
      </div>

      <div class="main-content">
          @yield('content')
      </div>

      <footer class="footer">
        <div class="w-100 clearfix">
          <span class="text-center text-sm-left d-md-inline-block">COPYRIGHT © 2019 PERTAMINA MEGAPROJECT.</span>
        </div>
      </footer>

    </div>
  </div>

  
  
  <script src="{{ asset('backend/plugins/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/screenfull/dist/screenfull.js') }}"></script>
  <script src="{{ asset('backend/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('backend/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/d3/dist/d3.min.js') }}"></script>
  <script src="{{ asset('backend/plugins/c3/c3.min.js') }}"></script>
  <script src="{{ asset('backend/js/plugins/tables.js') }}"></script>
  <script src="{{ asset('backend/js/plugins/widgets.js') }}"></script>
  <script src="{{ asset('backend/js/plugins/charts.js') }}"></script>
  <script src="{{ asset('backend/js/theme.js') }}"></script>
    
</body>

</html>
