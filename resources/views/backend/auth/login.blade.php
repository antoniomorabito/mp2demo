<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PERTAMINA MEGAPROJECT</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" href="{{ asset('img/p-icon.png') }}" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/ionicons/dist/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/icon-kit/dist/css/iconkit.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/css/theme.min.css')}}">
    <script src="{{ asset('backend/js/vendor/modernizr-2.8.3.min.js')}}"></script>
  </head>

  <style>
  
  .auth-wrapper .lavalite-bg .lavalite-overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: linear-gradient(145deg, rgba(1,1,1,0.5) 0%, rgba(1,1,1,.9) 100%);
  }

  .auth-wrapper .authentication-form .logo-centered {
    width: 180px;
    margin: 0 0;
    margin-bottom: 0px;
    margin-bottom: 40px;
  }

  .btn-wide{
    padding-left:25px;
    padding-right:25px;
    padding-bottom:25px;
  }

  </style>

<body>
  <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <div class="auth-wrapper">
    <div class="container-fluid h-100">
      <div class="row flex-row h-100 bg-white">
        <div class="col-md-8 p-0 d-md-block d-lg-block d-sm-none d-none">
          <div class="lavalite-bg" style="background-image: url('{{ asset('backend/img/14day_392.jpg')}}')">
            <div class="lavalite-overlay"></div>
          </div>
        </div>
        <div class="col-md-4 my-auto p-0">
          <div class="authentication-form mx-auto">
            <div class="logo-centered">
                <a href="../index.html"><img src="{{ asset('backend/img/logo1.png')}}" alt="" class="img-fluid"></a>
            </div>
            <h3 class="mb-1">Login to admin account</h3>
            @if(@Session::get('status')=='error')
              <p class="text-danger">{!! @Session::get('message') !!}</p>
            @else
              <p>Enter your username and password</p>
            @endif
            <form method="POST" action="{{ route('admin/login') }}" id="login_form">
            @csrf
              <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username" required="" value="">
                <i class="ik ik-user"></i>
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="" value="">
                <i class="ik ik-lock"></i>
              </div>
              <div class="sign-btn text-left">
                <button class="btn btn-primary btn-wide">{{ __('Sign In') }}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="{{ asset('backend/plugins/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{ asset('backend/plugins/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('backend/plugins/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
  <script src="{{ asset('backend/plugins/plugins/screenfull/dist/screenfull.js')}}"></script>
  <script src="{{ asset('backend/plugins/dist/js/theme.js')}}"></script>
      
  </body>
</html>

{{-- @extends('backend.layout')

@section('content')

  <!-- Page content -->
	<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Content area -->
      <div class="content">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-4">
              <div class="card">
                <div class="text-center">
                  
                  <h4><a class="p-4" href="#">PERTAMINA MEGAPROJECT</a></h4>
                  <h5 class="mb-1">Login to admin account</h5>
                 -- @if(@Session::get('status')=='error')
                    <span class="d-block text-danger">{!! @Session::get('message') !!}</span>
                  @else
                  <span class="d-block text-muted">Enter your username and password</span>
                  @endif
                </div>
                  <div class="card-body mb-2">
                    <form method="POST" action="{{ route('admin/login') }}" id="login_form">
                      @csrf
                      <div class="form-group row">
                        <div class="col-md-12">
                          <div class="form-group form-group-feedback form-group-feedback-left mb-0">
                            <input type="text" name="username" class="form-control" placeholder="{{ __('Username') }}" value="{{ old('username') }}" required>
                            <div class="form-control-feedback">
                              <i class="icon-user  text-muted"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-md-12">
                          <div class="form-group form-group-feedback form-group-feedback-left mb-0">
                            <input type="password" name="password" class="form-control" placeholder="{{ __('Password') }}" value="{{ old('password') }}" required>
                            <div class="form-control-feedback">
                              <i class="icon-lock text-muted"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group row mb-2">
                        <div class="col-md-12">
                          <button type="submit" id="submit_bt" class="btn btn-primary btn-block">
                            {{ __('Sign In') }}
                          </button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /content area -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->


  <script type="text/javascript">
    $(document).ready(function() {	
     
      $("#login_form").submit(function(){
        $("#submit_bt").attr("disabled","disabled");
        $(".form-control").attr("readonly","readonly");
        $("#submit_bt").html("Signing in...");
      });  
    });
  </script> 
    
@endsection --}}
