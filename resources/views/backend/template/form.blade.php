@extends( 'backend.layout' )

@section( 'content' )
<script>
(function($) {
  'use strict';
  $(function() {
    $('.file-upload-browse').on('click', function() {
      var file = $(this).parent().parent().parent().find('.file-upload-default');
      file.trigger('click');
    });
    $('.file-upload-default').on('change', function() {
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });
})(jQuery);
</script>

<div class="container-fluid">

  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="card">
        <form action="{{URL("admin/post/$ctrl/save")}}" method="POST" class="form-horizontal form-validate-jquery" id="transaction_form" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ @$detail->{$pk} }}" />
        <div class="card-header"><h3>{{ $page_title }}</h3></div>
        <div class="card-body">

          @if(in_array("module", $data_option))
          <div class="form-group">
            <label class="">Menu <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <select class="form-control" name="module_id" id="module_id" required>
                <option value=""  >Pilih menu...</option>
                @foreach($module_list as $m)
                <option value="{{$m->module_id}}" @if(@$detail->module==$m->module_id) {{"selected"}} @endif >{{ $m->title }}</option>
                @endforeach
              </select>
            </div>
          </div>
          @endif

          @if(in_array("name", $data_option))
          <div class="form-group">
            <label class="">Nama <span class="text-danger">*</span></label>
              <input class="form-control" type="text" name="name" value="{{@$detail->name}}" required>
          </div>
          <div class="form-group">
            <label class="">Nama En <span class="text-danger">*</span></label>
              <input class="form-control" type="text" name="name_en" value="{{@$detail->name_en}}" required>
          </div>  
          @endif

          @if(in_array("project", $data_option))
          <div class="form-group">
            <label class="">Nama Proyek <span class="text-danger">*</span></label>
              <select class="form-control w-50" name="project" id="project" required>
                <option value=""  >Pilih Proyek...</option>
                @foreach($project_list as $p)
                <option value="{{$p->id}}" @if(@$detail->project_id==$p->id) {{"selected"}} @endif >{{ $p->location_name }}</option>
                @endforeach
              </select>
          </div>
          @endif

          @if(in_array("menu", $data_option))
          <div class="form-group">
            <label class="">Menu <span class="text-danger">*</span></label>
              <select class="form-control w-50" name="menu" id="menu" required>
                <option value=""  >Pilih Menu...</option>
                @foreach($menu_list as $m)
                <option value="{{$m->id}}" @if(@$detail->menu==$m->id) {{"selected"}} @endif >{{ $m->name }}</option>
                @endforeach
              </select>
          </div>
          @endif

          @if(in_array("year", $data_option))
          <div class="form-group">
            <label class="">Tahun <span class="text-danger">*</span></label>
              <input class="form-control w-25" type="text" name="year" value="{{@$detail->year}}" required>
          </div>
          @endif

         

          
          @if(in_array("type", $data_option))
          <div class="form-group">
            <label class="">Jenis Galeri <span class="text-danger">*</span></label>
            <div class="col-md-3">
              <select class="form-control" name="type" id="gallery_type" required>
                <option value=""  >Pilih jenis galeri...</option>
                <option value="1" @if(@$detail->type=='1') {{"selected"}} @endif >Foto</option>
                <option value="2" @if(@$detail->type=='2') {{"selected"}} @endif >Video</option>
              </select>
            </div>
          </div>
          @endif

          @if(in_array("category", $data_option))
          <div class="form-group">
            <label class="">Kategori <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <select class="form-control" name="category" id="category" required>
                <option value=""  >Pilih kategori...</option>
                @foreach($category_list as $c)
                <option value="{{$c->id}}" @if(@$detail->category==$c->id) {{"selected"}} @endif >{{ $c->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          @endif

          @if(in_array("title", $data_option))
          <div class="form-group">
            <label class="">@if($ctrl=='gallery') {{"Caption"}} @else {{"Judul"}} @endif <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="title" value="{{@$detail->title}}" required>
          </div>  
          <div class="form-group">
            <label class="">@if($ctrl=='gallery') {{"Caption"}} @else {{"Judul En"}} @endif <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="title_en" value="{{@$detail->title_en}}" required>
          </div>  
          @endif

          @if(in_array("event_date", $data_option))
          <div class="form-group">
            <label class="" style="display: block">Tanggal Event <span class="text-danger">*</span></label>
              <input class="form-control w-25" style="display: inline" type="date" name="date_start" value="{{@$detail->start_date}}" required placeholder="Tanggal Mulai"> - 
              <input class="form-control w-25" style="display: inline" type="date" name="date_end" value="{{@$detail->end_date}}" required  placeholder="Tanggal Selesai">
          </div>
          @endif

          @if(in_array("location", $data_option))
          <div class="form-group">
            <label class="">Lokasi Event <span class="text-danger">*</span></label>
              <input class="form-control" type="text" name="location" value="{{@$detail->location}}" required>
          </div>
          @endif

          @if(in_array("url_view", $data_option))
          <div class="form-group" id="url_view_div">
            <label class="">URL View <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="url_view" id="url_view" value="{{@$detail->url_view}}" required>
            </div>
          </div>
          @endif

          @if(in_array("url", $data_option))
          <div class="form-group" id="url_view_div">
            <label class="">URL <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <input class="form-control" type="text" name="url" id="url" value="{{@$detail->url}}" required>
            </div>
          </div>
          @endif
                  
                  
          @if(in_array("content_short", $data_option))                   
          <div class="form-group">
              <label class="">Konten Singkat <span class="text-danger">*</span></label>
              <textarea class="form-control" rows="3"  maxlength="200" type="text" name="content_short" required>{{@$detail->content_short}}</textarea>
          </div>
          <div class="form-group">
            <label class="">Konten Singkat En <span class="text-danger">*</span></label>
            <textarea class="form-control" rows="3"  maxlength="200" type="text" name="content_short_en" required>{{@$detail->content_short_en}}</textarea>
          </div>
          @endif

          @if(in_array("content_full", $data_option)) 
            @if($ctrl=='contact') 
              @php $detail_label = "Pesan"; $rows=10; $disabled="disabled"; $required=""; $id="" @endphp
            @else 
              @php $detail_label = "Konten Detail"; $rows=10; $disabled=""; $id="editor1"; $required='<span class="text-danger">*</span>' @endphp
              @php $detail_label_en = "Konten Detail En"; $rows=10; $disabled=""; $id="editor1"; $required='<span class="text-danger">*</span>' @endphp
            @endif
          
          <div class="form-group">
            <label class="">{{$detail_label}} {!!$required!!}</label>
            <textarea id="{{$id}}"  class="form-control content_full" rows="{{$rows}}" type="text" name="content_full" >{{@$detail->content_full}}</textarea>
          </div>

          <div class="form-group">
            <label class="">{{$detail_label_en}} {!!$required!!}</label>
            <textarea id="{{$id}}"  class="form-control content_full" rows="{{$rows}}" type="text" name="content_full_en" >{{@$detail->content_full_en}}</textarea>
          </div>
          @endif


          @if(in_array("question", $data_option))                   
          <div class="form-group">
            <label class="">Pertanyaan <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <textarea class="form-control" rows="3" type="text" name="question" required>{{@$detail->question}}</textarea>
            </div>
          </div>
          @endif

          @if(in_array("answer", $data_option)) 
          <div class="form-group">
            <label class="">Jawaban <span class="text-danger">*</span></label>
            <div class="col-md-9">
              <textarea id="editor2"  class="form-control" rows="15" type="text" name="answer" required>{{@$detail->answer}}</textarea>
            </div>
          </div>
          @endif


          @if(in_array("image", $data_option) and isset($detail) ) 
          <div class="form-group" id="image_prev_div">
            <label class="">@if($ctrl=='menu') Icon @else Foto @endif @if(!isset($detail)) {!! '<span class="text-danger">*</span>' !!} @endif </label>
              <div class="col-md-2 pl-0">
                  @if($detail->image=='')
                  <img src="{{ URL('public/images/no-image.png') }}" alt="" class="img-fluid" >   
                @else
                  <img class="img-fluid" src="{{URL('public/images/'.$ctrl.'/'.$detail->image)}}" />
                @endif
              </div>
          </div>
          @endif

          @if(in_array("image", $data_option)) 
          <div class="form-group" id="image_div">
            <label class="">@if(isset($detail)) {{"Update "}} @endif @if($ctrl=='menu') Icon @else Foto @endif @if(!isset($detail)) {!! '<span class="text-danger">*</span>' !!} @endif </label>
              {{-- <input class="form-control" style="margin-bottom: 10px" type="file" id="image" name="image" value="{{@$detail->image}}" @if(!isset($detail)) {{ "required" }} @endif /> --}}
              <input type="file" name="image" class="file-upload-default">
              <div class="input-group col-xs-12">
                <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                <span class="input-group-append">
                <button class="file-upload-browse btn btn-inverse" type="button">Upload</button>
                </span>
              </div>

              {{-- @if($ctrl=='article')
              <small class="text-muted">Photo size : 650px x 650px</small>
              @endif --}}
              {{-- @if($ctrl=='reference' or $ctrl=='guide')
              <small class="text-muted">Photo size : 500px x 650px</small>
              @else
              <small class="text-muted">Photo size : 770px x 380px</small>
              @endif --}}
          </div>
          
          @endif

          
          @if($ctrl=='gallery')
            @php $label = "File"; @endphp
          @else
            @php $label = "Dokumen"; $label_en = "Dokumen En" @endphp
          @endif

          @if(in_array("file", $data_option) and isset($detail) ) 
          <div class="form-group" id="div_prev_file">
            <label class="">{{$label}}  @if(!isset($detail)) {!! '<span class="text-danger">*</span>' !!} @endif </label>
            <div class="col-md-9">
              <a href="{{URL("public/docs/$ctrl/$detail->file")}}" />{{$detail->file}}</a>
            </div>
          </div>
          <div class="form-group" id="div_prev_file">
            <label class="">{{$label_en}}  @if(!isset($detail)) {!! '<span class="text-danger">*</span>' !!} @endif </label>
            <div class="col-md-9">
              <a href="{{URL("public/docs/$ctrl/$detail->file_en")}}" />{{$detail->file_en}}</a>
            </div>
          </div>
          @endif

          @if(in_array("file", $data_option))
          <div class="form-group" id="div_file">
            <label class="">@if(isset($detail)) {{ "Update" }} @else {{ "Upload" }} @endif {{$label}} @if(!isset($detail)) {!! '<span class="text-danger">*</span>' !!} @endif </label>
            <div class="col-md-9">
              <input class="form-control" id="file" type="file" name="file" value="{{@$detail->file}}" @if(!isset($detail)) {{ "required" }} @endif />
            </div>
          </div>
          <div class="form-group" id="div_file">
            <label class="">@if(isset($detail)) {{ "Update" }} @else {{ "Upload" }} @endif {{$label_en}} @if(!isset($detail)) {!! '' !!} @endif </label>
            <div class="col-md-9">
              <input class="form-control" id="file_en" type="file" name="file_en" value="{{@$detail->file_en}}" @if(!isset($detail)) {{ "" }} @endif />
            </div>
          </div>
          @endif

          @if(in_array("video_url", $data_option))
          <div class="form-group" id="div_video_url">
            <label class="">Youtube ID</label>
            <div class="col-md-9">
              <input class="form-control" type="text"  style="margin-bottom: 10px" placeholder="https://www.youtube.com/watch?v={Youtube ID}" id="video_url" name="video_url" value="{{@$detail->video_url}}" >
              <small  class="text-muted">Isi hanya Youtube ID saja, tidak dengan alamat youtubenya</small>
            </div>
          </div>  
          @endif

          @if(in_array("dot_color", $data_option))
          <div class="form-group">
            <label class="">Warna Dot Timeline <span class="text-danger">*</span></label>
              <select class="form-control w-25" name="dot_color" id="dot_color" required>
                <option value=""  >Pilih Warna...</option>
                <option value="1" @if(@$detail->dot_color=='1') {{"selected"}} @endif >Merah</option>
                <option value="0" @if(@$detail->dot_color=='0') {{"selected"}} @endif >Putih</option>
              </select>
          </div>
          @endif
           
        </div>
        <div class="card-footer">
            <button type="submit" id="submit_bt" class="btn btn-primary btn-wide mr-2">Simpan</button>
            <a type="button" href="{{URL("admin/post/$ctrl")}}" class="btn btn-light btn-wide">Batal</a>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
    

@endsection
