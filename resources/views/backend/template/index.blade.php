@extends( 'backend.layout' )

@section( 'content' )

<script>
$(function () {
  $(document).on('click', ".delete_bt", function () {
      delete_url = $(this).attr('delete_url');
      var r = confirm("Hapus data ini?");
      if (r == true) {
          window.location = delete_url;
      }
    });
});
</script>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row card-header">
                  <div class="col-md-6">
                    <h3 class="pt-2">{{ $page_title }}</h3>
                  </div>
                  @if($ctrl!='vision-mission' and $ctrl!='org-structure' and $ctrl!='help' and $ctrl!='menu' and $ctrl!='6c')
                  <div class="col-md-6 text-right"><a type="button" href="{{URL("admin/post/$ctrl/create")}}" class="btn btn-primary btn-wide">Tambah Data</a></div>
                  @endif
                </div>
                <div class="card-body">

                    @if(@Session::get('status')=='success')
                    <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        {!! @Session::get('message') !!}
                    </div>
                    @endif
                    
                    @if(@Session::get('status')=='error')
                    <div class="alert bg-danger text-white alert-styled-left alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        {!! @Session::get('message') !!}
                    </div>
                    @endif
                    
                    <table id="" class="table table-striped">
                        <thead>
                            <tr>
                                <th width="5%" class="text-center" >No</th>
                                @if(in_array("image", $table_option))
                                <th width="10%" style="text-align: center">@if($ctrl=='menu') Icon @else Foto @endif</th>
                                @endif
                                @if(in_array("name", $table_option))
                                <th width="20%">Nama @if($ctrl=='project-profile') Proyek @endif </th>
                                @endif
                                @if(in_array("year", $table_option))
                                <th width="10%" class="text-center">Tahun</th>
                                @endif
                                @if(in_array("title", $table_option))
                                <th width="40%">Judul @if($ctrl=='event') Event @endif</th>
                                @endif
                                @if(in_array("content_short", $table_option))
                                <th width="40%">Konten</th>
                                @endif
                                @if(in_array("category", $table_option))
                                <th width="20%">Kategori</th>
                                @endif
                                @if(in_array("module", $table_option))
                                <th width="20%">Menu</th>
                                @endif

                                @if(in_array("menu", $table_option))
                                <th width="20%">Menu</th>
                                @endif

                                @if(in_array("project", $table_option))
                                <th width="10%">Proyek</th>
                                @endif

                                @if(in_array("subject", $table_option))
                                <th width="30%">Subjek</th>
                                @endif
                                @if(in_array("question", $table_option))
                                <th width="40%">Pertanyaan</th>
                                @endif
                                @if(in_array("pilar", $table_option))
                                <th width="10%" style="text-align: center">Pilar</th>
                                @endif
                                @if(in_array("event_date", $table_option))
                                <th width="15%" style="text-align: center">Tanggal Event</th>
                                @endif
                                <th width="15%" style="text-align: center">Tanggal Input</th>
                                @if($ctrl=='news')
                                <th width="10%">Penulis</th>
                                @endif
                                {{-- <th width="15%" style="text-align: center" >Status</th> --}}
                                @if(in_array("headline", $table_option))
                                <th width="10%">Headline</th>
                                @endif
                                <th width="10%" style="text-align: center"> @if($ctrl!='news-letter') {{"Aksi"}} @endif</th>
                              </tr>
                        </thead>
                        <tbody>
                            @if($list!=NULL)
                        @foreach($list as $row)
                        @php $no = ($page-1)*30; @endphp
                          <tr>
                            @if($ctrl=='news')
                            <td  class="text-center" style="vertical-align: middle" >{{$loop->iteration+$no}}</td>
                            @else
                            <td  class="text-center" style="vertical-align: middle">{{$loop->iteration}}</td>
                            @endif
                            @if(in_array("image", $table_option))
                            <td class="text-center" style="vertical-align: top">
                              @if($row->image=='')
                            <img src="{{ URL('public/images/no-image.png') }}" alt="" class="img-fluid">   
                            @else
                              @if(@$row->from_wp==0)
                              <img src="{{ URL('public/images/'.$ctrl.'/'.$row->image) }}" alt="" class="img-fluid">
                              @else
                              @php $year_post = substr($row->created_at,0,4); $month_post = substr($row->created_at,5,2); @endphp
                              <img src="{{ URL('public/images/wp/'.$year_post.'/'.$month_post.'/'.$row->image) }}" alt="" class="img-fluid">
                              @endif
                            @endif  
                            </td>
                            @endif
                            @if(in_array("name", $table_option))
                            <td style="vertical-align: middle">{{$row->name}}</td>
                            @endif
                            @if(in_array("year", $table_option))
                            <td class="text-center" style="vertical-align: middle">{{$row->year}}</td>
                            @endif
                            @if(in_array("title", $table_option))
                            <td style="vertical-align: middle">
                              {!! "<b>".ucfirst(str_replace("_","-",$row->title))."</b>" !!}
                              @if(@$row->content_short!=NULL or @$row->content_short!='')
                              <p>{!! $row->content_short !!}</p>
                              @endif
                            </td>
                            @endif
                            @if(in_array("content_short", $table_option))
                            <td>
                              {!! $row->content_short !!}
                            </td>
                            @endif
                            @if(in_array("project", $table_option))
                            <td style="vertical-align: middle">{{getProjectName($row->project_id)}}</td>
                            @endif

                            @if(in_array("category", $table_option))
                            <td style="vertical-align: middle">{{getCategoryName($row->category)}}</td>
                            @endif
                            @if(in_array("module", $table_option))
                            <td style="vertical-align: middle">{{getModuleName($row->module)}}</td>
                            @endif
                            @if(in_array("menu", $table_option))
                            <td style="vertical-align: middle">{{getMenuName($row->menu)}}</td>
                            @endif
                            @if(in_array("subject", $table_option))
                            <td style="vertical-align: middle">{{$row->subject}}</td>
                            @endif

                            @if(in_array("question", $table_option))
                            <td style="vertical-align: middle">{{$row->question}}</td>
                            @endif

                            @if(in_array("pilar", $table_option))
                            <td style="vertical-align: middle;text-align: center">{{$row->pilar }}</td>
                            @endif

                            @if(in_array("event_date", $table_option))
                            <td style="vertical-align: middle;text-align: center">{{$row->start_date." ‒ ".$row->end_date }}</td>
                            @endif

                            <td style="vertical-align: middle;text-align: center" style=""><?php echo substr($row->created_at,0,10) ?></td>

                            @if($ctrl=='news')
                            <td style="vertical-align: middle">{{$row->user_name}}</td>
                            @endif
                            
                            {{-- @if($ctrl=='contact')
                                
                            <td class="text-center" style="vertical-align: middle">{!!get_contact_status($row->status)!!}</td>
                            @elseif($ctrl=='news-ticker')
                                @if($loop->iteration<=3)
                                    @php $status = 1 @endphp
                                @else
                                    @php $status = 2; @endphp
                                @endif
                            <td class="text-center" style="vertical-align: middle">{!!getPublishStatus($status)!!}</td>
                            @else
                            <td class="text-center status_label" row_id="{{ $row->{$pk} }}" style="vertical-align: middle">{!!getPublishStatus($row->status)!!}</td>
                            <td class="text-center status_ddl" row_id="{{ $row->{$pk} }}" style="vertical-align: middle;display: none">
                                    <select class="form-control" name="status_{{ $row->{$pk} }}" id="status_{{ $row->{$pk} }}" required>
                                        <option value="0" @if(@$row->status=='0') {{"selected"}} @endif >Draft</option>
                                        <option value="1" @if(@$row->status=='1') {{"selected"}} @endif >Publish</option>
                                        <option value="2" @if(@$row->status=='2') {{"selected"}} @endif >Hide</option>
                                    </select>
                            </td>

                            @endif --}}

                            @if(in_array("headline", $table_option))
                            <td class="text-center headline_label" row_id="{{ $row->{$pk} }}" style="vertical-align: middle"><span class="label label-warning" style="font-size: 12px;padding:4px 10px">{{$row->headline}}</span></td>
                            <td class="text-center headline_input" row_id="{{ $row->{$pk} }}" style="vertical-align: middle;display: none"><input type="text" name="headline_{{ $row->{$pk} }}" id="headline_{{ $row->{$pk} }}" class="form-control headline text-center"  maxlength="1" value="{{$row->headline}}" /></td>
                            @endif

                            <td>
                              <div class="table-actions text-center">
                                {{-- <i href="#"><i class="ik ik-eye"></i></i> --}}
                                <a href="{{ URL('admin/post/'.$ctrl.'/update/'.$row->{$pk}) }}"><i class="ik ik-edit-2"></i></a>
                                @if($ctrl!='vision-mission'  and $ctrl!='org-structure'  and $ctrl!='help' and $ctrl!='menu' and $ctrl!='6c')
                                <a class="delete_bt" delete_url="{{ URL('admin/post/'.$ctrl.'/delete/'.$row->{$pk}) }}" href="javascript:void(0)"><i class="ik ik-trash-2"></i></a>
                                @endif
                              </div>
                            </td>

                            {{-- <td class="text-left" style="vertical-align: middle">
                              <ul class="icons-list" style="list-style: none">
                                @if(in_array("file", $table_option) and $row->file!='')
                                <li title="Download"><a href="{{URL("public/docs/$ctrl/$row->file")}}">Download</i></a></li>
                                @endif
                                @if($ctrl=='contact' )
                                <li title="Balas"><a href="{{ URL('admin/post/'.$ctrl.'/update/'.$row->{$pk}) }}"><i class="icon-reply"></i></a></li>
                                @endif
                                @if($ctrl!='news-letter' and $ctrl!='contact')
                                <li title="Edit"><a href="{{ URL('admin/post/'.$ctrl.'/update/'.$row->{$pk}) }}">Edit</a></li>
                                @endif
                                @if($ctrl!='about-gain' and $ctrl!='about-baduta' and $ctrl!='contact-us' and $ctrl!='make-your-emo-demo' ) 
                                <li title="Hapus"><a class="delete_bt" delete_url="{{ URL('admin/post/'.$ctrl.'/delete/'.$row->{$pk}) }}" href="javascript:void(0)" >Delete</a></li>
                                @endif
                                @if($ctrl=='article')
                                <li title="Edit"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Slider Banner</a></li>
                                @endif
                              </ul>
                            </td> --}}
                          </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
