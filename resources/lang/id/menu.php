<?php

// Indonesia

return [
    'home' => 'Beranda',
    'vision-mission' => 'Visi Misi',
    'csr' => 'CSR',
    'project' => 'Proyek',
    'project-profile' => 'Profil Proyek',
    'project-status' => 'Status Proyek',
    'org-structure' => 'Struktur Organisasi',
    'directorate-policy' => 'Kebijakan Direktorat',
    'calendar' => 'Kalender',
    'help'=>'Bantuan',
    'project-location' => 'Lokasi Proyek',
    'upcoming-events' => 'Acara Mendatang',
];
