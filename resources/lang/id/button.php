<?php

// Indonesia

return [
	
	'save' => 'Simpan',
	'cancel' => 'Batal',
	'back' => 'Kembali',
	'close' => 'Keluar',
	'edit_profile_data' => 'Edit Data Profil',
	'add' => 'Tambah Data', 
	'edit' => 'Ubah Data',
	'delete' => 'Hapus Data',
	'data_profile' => 'Data Profil',
	'data_list' => 'List Data',
	'print' => 'Cetak',
	
];