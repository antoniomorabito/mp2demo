<?php

// Indonesia

return [
		
  'has-been-added' => '<b>Success!</b> Data :title telah ditambahkan',
  'has-been-updated' => '<b>Success!</b> Data :title telah diubah',
  'has-been-deleted' => '<b>Success!</b> Data :title telah dihapus',
  'error-message' => '<b>Error!</b> :message',
  
];