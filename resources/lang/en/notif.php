<?php

// English

return [
		
  'has-been-added' => '<b>Success!</b> Data :title has been added',
  'has-been-updated' => '<b>Success!</b> Data :title has been updated',
  'has-been-deleted' => '<b>Success!</b> Data :title has been deleted',
  'error-message' => '<b>Error!</b> :message',
  
];