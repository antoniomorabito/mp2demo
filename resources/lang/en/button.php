<?php

// English

return [
	  
	'save' => 'Save',
	'cancel' => 'Cancel',
	'back' => 'Back',
	'close' => 'Close',
	'edit_profile_data' => 'Edit Profile Data',
	'add' => 'Create New', 
	'edit' => 'Edit Data',
	'delete' => 'Delete Data',
	'data_profile' => 'Profile Data',
	'data_list' => 'Data List',
	'print' => 'Print',
	
];