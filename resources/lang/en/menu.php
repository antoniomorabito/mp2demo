<?php

// English

return [
    'home' => 'Home',
    'vision-mission' => 'Vision Mission',
    'csr' => 'CSR',
    'project' => 'Project',
    'project-profile' => 'Project Profile',
    'project-status' => 'Project Status',
    'org-structure' => 'Organization Structure',
    'directorate-policy' => 'Directorate Policy',
    'calendar' => 'Calendar',
    'help'=>'Help',
    'project-location' => 'Project Location',
    'upcoming-events' => 'Upcoming Events'
];
